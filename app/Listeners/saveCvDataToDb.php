<?php

namespace App\Listeners;

use App\Events\CvCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue; // no need for queue yet
use App\CvUser;
use Auth;

class saveCvDataToDb
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CvCreated  $event
     * @return void
     */
    public function handle(CvCreated $event)
    {
        // for now save only cv_path then save rest data in $event->cv_sections_data
        $new_cv = new CvUser();
        $new_cv->user_id = Auth::id();
        $new_cv->cv_path = $event->cv_sections_data['cv_path'];
        $new_cv->save();
    }
}
