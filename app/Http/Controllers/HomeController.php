<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDF;
use App\Events\CvCreated;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function list()
    {
        // $pdf = PDF::loadView('novo', []);
        $list = Auth::user()->userCvs;

        return view('cv.list', compact('list'));
    }


    /**
     *
     * save cv and data to db
     */
    public function save (Request $request) {
        $cv_sections_data = $request->all();

        $messages = [
            'required' => 'The :attribute field is required.',
            'email' => 'The :attribute field shoud be email.',
            'digits' => 'The :attribute field should consists only digits.',
        ];
        // when passed vue basic validation, validate on server side for some fields
        $validator = Validator::make($cv_sections_data, [
            "contacts.*.contact_email"  => "required|email|",
            "contacts.*.contact_phone"  => "required|digits:9|",
            "skills.*.skill_name"  => "required|min:3",
            "work_experience.*.company_name"  => "required|min:3",
            "work_experience.*.work_position"  => "required|min:3",
            "personal_info.*.name"  => "required|min:3",
            "personal_info.*.family"  => "required|min:3",
            "personal_info.*.profesional_title"  => "required|min:3",
            "personal_info.*.about"  => "required|min:3",
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();;
        }
        else {        
            if (!is_dir(public_path().'/cv/')) mkdir(public_path().'/cv/');
            if (!is_dir(public_path().'/cv/'.Auth::user()->name).'/') mkdir(public_path().'/cv/'.Auth::user()->name.'/');
           
            $cv_name = 'cv_'.date("H_i_s").'.pdf';

            $pdf = PDF::loadView('pdf', compact('cv_sections_data'))->save(public_path().'/cv/'.Auth::user()->name.'/'.$cv_name);

            // add user folder 
            $cv_sections_data['cv_path'] = '/cv/'.Auth::user()->name.'/'.$cv_name;

            // save cv data in db only when file is saved to th project
            event(new CvCreated($cv_sections_data));

            // see pdf while it saved
            return $pdf->stream($cv_name);
        }
    }
}
