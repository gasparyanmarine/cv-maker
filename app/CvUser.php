<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvUser extends Model
{
	protected $table = 'cv_user';
	/**
	* no need to be mass assignable, will use save()
	*/
    // protected $fillable = [
    //     'user_id', 'cv_path'
    // ];


}
