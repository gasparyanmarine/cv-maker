@extends('cv.layouts.app')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        <strong class="card-title">CV Maker</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Create your CV</strong>
                    </div>
                    <div class="card-body" id="cv-template">
                        <form id="cv-create-form" action="{{ URL::to('/save') }}" method="POST"
                         v-on:submit.prevent="onSubmit">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @csrf
                            @include('cv.sections.header')
                            @include('cv.sections.contact')
                            @include('cv.sections.experience')
                            <!-- Will not include yet -->
                            <!-- @include('cv.sections.education') --> 
                            @include('cv.sections.skills')

                            <div class="row form-group" id="create-section">
                                <div class="col-3 d-flex align-center m-auto">
                                    <button type="submit" class="btn btn-xs btn-info w-75" >Create</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var app = new Vue({
            el: '#cv-create-form',
            data: {
                // describe sections
                personal_info: [{name: '', family: '', profesional_title: '', about: ''}],
                contacts: [{contact_email: '', contact_phone: ''}],
                work_experience: [{company_name: '', work_position: ''}], //initially we have 1 block and with empty values
                skills: [{skill_name: ''}],
                error: {}
            },
            methods: {
                onSubmit: function (event) {
                    // check if there is empty input
                    let errors = {};
                    for (block_name in this.$data) { 
                        
                        if (block_name !== 'error') {

                            this.$data[block_name].map((value, index) => {
                                // value.length < 3;
                                Object.values(value).filter(value => {

                                    if( !this.checkTextLength(value)) {
                                        errors[block_name] =  ['Please write something more :)'];
                                    }
                                });
                            });
                        } 
                        
                    }
                  
                    this.error = errors;

                    if (Object.values(errors).length || $('.error').length) {
                        // renders and sets the errors in dom a bit later 
                        setTimeout(function(){
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(".error").offset().top-100
                            }, 2000);
                        }, 0)
                        
                        return
                    } 
                    
                    $(this.$el).submit();
                    
                }, 
                addBlock: function (event, block_name, ...fields) {
                    // make object like initial one
                    let newBlock = new Object();
                    fields.map(field => newBlock[field] = '');
                    // push one more empty block 
                    this[block_name].push(newBlock);
                },
                removeBlock: function (event, block_name, block_index) {
                    // remove added block by index
                    this[block_name].splice(block_index, 1);
                },
                changeBlockFieldValue: function (event, block_name, block_item_index, field_name) {
                    let field_value = event.target.value;
                    this.error = {}; //empty all errors for current block
                    this[block_name][block_item_index][field_name] = event.target.value;

                    // basic validation
                    if( (field_name === 'contact_email')  || (field_name === 'contact_phone')) {
                        console.log(!this[`validate${field_name}`](field_value))
                        if( !this[`validate${field_name}`](field_value))  //call validate for each field 
                        {
                            this.error[block_name] = [];
                            
                            if (field_name === 'contact_phone') { 
                                this.error[block_name] = [ 'Phone number can include only digits, and should be with 9 length'];
                            } else {
                                this.error[block_name] = ['Incorrect input data'];
                            }
                        }
                    } else {
                        // for rest field will check only min length
                        if( !this.checkTextLength(field_value)) {
                            this.error[block_name] = [] ;
                            this.error[block_name] = [ 'Please write something more :)'];
                        }
                    }
                    
                },
                validatecontact_email: function (field_value) {
                    let email = field_value;
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email).toLowerCase()); 
                },
                validatecontact_phone: function (field_value) {
                    // let phoneNumber = field_value.replace(/\D+/ig, ''); // leave only digits
                    return /^([0-9]{9,9})$/.test(field_value);
                },
                checkTextLength: function (field_value) {
                    return field_value.length >= 3;
                }

            }
        })
    })
</script>
@endpush