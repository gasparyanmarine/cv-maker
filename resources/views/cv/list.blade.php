@extends('cv.layouts.app')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        <strong class="card-title">CV lists</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <ul class="list-group">
                        @foreach($list as $cv)
                            <li class="list-group-item"> <a href="{!! $cv->cv_path !!}" download="">{{$cv->cv_path}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
