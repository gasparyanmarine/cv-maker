<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header mt-5 mb-3">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand pb-3" href="{{ URL::to('/home') }}"><img src="{{asset('images/cv.png')}}" width="80" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="{{asset('images/cv.png')}}" width="80" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{route('home') }}" class="nav-link"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ URL::to('/cv-list') }}" class="nav-link"> <i class="menu-icon fa fa-file"></i>See your CVs</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->