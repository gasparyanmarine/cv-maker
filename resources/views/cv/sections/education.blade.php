<h5 class="presection-header">Education</h5>

<div class="row form-group" id="personal-section">
	<div class="col-10 d-flex align-center">
		<div class="form-group col-4">
			<label for="name" class="form-control-label">Place of education</label>
			<input type="text" name="place_education[]" id="place_education" placeholder="Place of education" class="form-control" value="{{ old('vuejs') ?? ''}}">
		</div>
		<div class="form-group col-4">
			<label for="family" class="form-control-label">Education name</label>
			<input type="text" name="education_name[]" id="education_name" placeholder="Education name" class="form-control" value="{{ old('family') ?? ''}}">
		</div>
		<div class="form-group col-2 m-auto-0 text-left">
			<button type="button" class="btn btn-xs btn-info">+</button>
		</div>    
    </div>
</div>

