<h5>Contact Info</h5>
<div class="error alert alert-danger" v-if="error.contacts">
	<ul>
	    <li v-for="(error, index) in error.contacts">@{{error}}</li>
	</ul>
</div>
<div class="row form-group" id="contact-section">
	<div class="col-10"  v-for="(contact, index) in contacts" v-bind:key="index">
		<div class="form-group col-6">
			<label for="email" class="form-control-label">Email</label>
			<input type="text" 
				placeholder="Enter your email" class="form-control" 
				v-bind:name="'contacts['+index+'][contact_email]'"
				v-bind:id="'contact_email'+index"
				v-bind:value="contact.contact_email"
				v-on:change="changeBlockFieldValue($event, 'contacts', index, 'contact_email')"
			>
		</div>
		<div class="form-group col-6">
			<label for="phone" class="form-control-label">Phone</label>
			<input type="text" 
				placeholder="Phone number consists of 9 number" class="form-control"
				v-bind:name="'contacts['+index+'][contact_phone]'" 
				v-bind:id="'contact_phone'+index"
				v-bind:value="contact.contact_phone"
				v-on:change="changeBlockFieldValue($event, 'contacts', index, 'contact_phone')"
			>
		</div>    
    </div>
</div>
