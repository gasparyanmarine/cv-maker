<h5 class="presection-header">WORK EXPERIENCE</h5>

<div class="error alert alert-danger" v-if="error.work_experience">
	<ul>
	    <li v-for="(error, index) in error.work_experience">@{{error}}</li>
	</ul>
</div>

<div class="row form-group" id="personal-section">
	<!-- Start WORK EXPERIENCE block -->
	<div class="col-10 d-flex align-center" v-for="(experience, index) in work_experience" v-bind:key="index">
		<div class="form-group col-5">
			<label v-bind:for="'company_name'+index" class="form-control-label">Company name</label>
			<input type="text"  
				placeholder="Company name" class="form-control" 
				v-bind:name="'work_experience['+index+'][company_name]'"
				v-bind:id="'company_name'+index" 
				v-bind:value="experience.company_name" 
				v-on:change="changeBlockFieldValue($event, 'work_experience', index, 'company_name')"
			>
		</div>
		<div class="form-group col-5">
			<label  v-bind:for="'work_position'+index" class="form-control-label">Work position</label>
			<input type="text"  
				placeholder="Fill work position name" class="form-control"
				v-bind:name="'work_experience['+index+'][work_position]'"
				v-bind:id="'work_position'+index" 
				v-bind:value="experience.work_position"
				v-on:change="changeBlockFieldValue($event, 'work_experience', index, 'work_position')"
			>
		</div>
		<div class="form-group col-2 m-auto-0 text-left" v-if="index>=1">
			<button type="button" class="btn btn-xs btn-danger" v-on:click="removeBlock($event, 'work_experience', index)">X</button>
		</div>
	</div>    
	<div class="form-group col-2 m-auto-0 text-left">
		<button type="button" class="btn btn-xs btn-info" v-on:click="addBlock($event, 'work_experience', 'company_name', 'work_position')" title="Add one more">+</button>
	</div>
	<!-- End WORK EXPERIENCE block -->
  
</div>


