<h5 class="presection-header">Skills</h5>
<div class="error alert alert-danger" v-if="error.skills">
	<ul>
	    <li v-for="(error, index) in error.skills">@{{error}}</li>
	</ul>
</div>

<div class="row form-group" id="personal-section">
	<div class="col-10 d-flex align-center">
		<div class="col-4 d-flex align-center" v-for="(skill, index) in skills" v-bind:key="index">
			<div class="form-group col-10">
				<label for="name" class="form-control-label">Skill name</label>
				<input type="text" placeholder="Skill name" class="form-control"
					v-bind:name="'skills['+index+'][skill_name]'"
					v-bind:id="'skill_name'+index" 
					v-bind:value="skill.skill_name"
					v-on:change="changeBlockFieldValue($event, 'skills', index, 'skill_name')"
				>
			</div>
			<div class="form-group col-1 m-auto-0 text-left" v-if="index>=1">
				<button type="button" class="btn btn-xs btn-danger" v-on:click="removeBlock($event, 'skills', index)">X</button>
			</div>
	    </div>
		<div class="form-group col-1 m-auto-0 text-left">
			<button type="button" class="btn btn-xs btn-info" v-on:click="addBlock($event, 'skills', 'skill_name')" title="Add one more">+</button>
		</div>    
    </div>
</div>

