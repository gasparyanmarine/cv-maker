<h5>Personal Info</h5>
<!-- show errors if exist -->
<div class="error alert alert-danger" v-if="error.personal_info">
	<ul>
	    <li v-for="(error, index) in error.personal_info">@{{error}}</li>
	</ul>
</div>
<div class="row form-group" id="personal-section">
	<div class="col-10" v-for="(personal, index) in personal_info">
		<div class="form-group col">
			<label for="name" class="form-control-label">First name</label>
			<input type="text"  placeholder="Your first name" class="form-control" 
				v-bind:name="'personal_info['+index+'][name]'" 
				v-bind:id="'name'+index" 
				v-bind:value="personal.name"
				v-on:change="changeBlockFieldValue($event, 'personal_info', index, 'name')"
			>
		</div>  
		<div class="form-group col">
			<label for="family" class="form-control-label">Family</label>
			<input type="text" placeholder="Family name" class="form-control" 
				v-bind:name="'personal_info['+index+'][family]'" 
				v-bind:id="'family'+index" 
				v-bind:value="personal.family"
				v-on:change="changeBlockFieldValue($event, 'personal_info', index, 'family')"
			>
		</div>
		<div class="form-group col">
			<label for="professional" class="form-control-label">Professional title</label>
			<input type="text"  placeholder="Professional title" class="form-control" 
				v-bind:name="'personal_info['+index+'][profesional_title]'" 
				v-bind:id="'profesional_title'+index" 
				v-bind:value="personal.profesional_title"
				v-on:change="changeBlockFieldValue($event, 'personal_info', index, 'profesional_title')"
			>
		</div>    
	    <div class="form-group col">
			<label for="about" class="form-control-label">About you</label>
			<textarea placeholder="About you" class="form-control"
				v-bind:name="'personal_info['+index+'][about]'" 
				v-bind:id="'about'+index" 
				v-bind:value="personal.about"
				v-on:change="changeBlockFieldValue($event, 'personal_info', index, 'about')"
			></textarea>
		</div>	
    </div>
</div>
