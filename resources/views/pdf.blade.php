<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CV Maker system</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="CV maker">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('assets/css/pdf.css') }}">


    <style type="text/css">
        .form-control-label {
            font-weight: 600;
            color: #4f90cd91;
            font-size: 20px
            padding: 0 5px;
        }
       .presection-header {
            color: #4f90cd;
            font-size: 25px;
            font-weight: 900;
            margin: 15px 15px 30px 15px;
            text-align: center;

        }
    
       {{-- 
        .col-12{
            position: relative;
            min-height: 50px;
            padding-right: 15px;
            padding-left: 15px;
            max-width: 100%;
            margin: 15px 0;
            width: 100%
        }
        .form-group {
            margin-bottom: 1rem;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
             width: 100%;
             height: auto;
        }
        .col-12{
            width: 100%;
            position: relative;
            display: block;
            margin: 15px 0;
            min-height: 50px;
            height: auto;
        }
        div[class*="col-"] {
            display: block;
            float: left;
        }

       /*
        */


       --}}
    </style> 


    

</head>

<body>


    @if(isset($cv_sections_data['personal_info']))
        <div class="row form-group" id="personal_info">
            <h5 class="presection-header">Personal Info</h5>
            @foreach($cv_sections_data['personal_info'] as $field_name => $personal_info)
                <div class="col-12" >
                    <p><label class="form-control-label">Name</label> <span> {{$personal_info['name']}} </span></p>
                    <p><label class="form-control-label">Family</label> <span>  {{$personal_info['family']}} </span></p>
                    <p><label class="form-control-label">Profesional_Title</label> <span> {{$personal_info['profesional_title']}} </span></p>
                    <p><label class="form-control-label">About</label> <span>  {{$personal_info['about']}} </span></p>
          
                </div>
            @endforeach
        </div>
    @endif

    @if(isset($cv_sections_data['contacts']))
        <div class="row form-group" id="contacts">
            <h5 class="presection-header">Contacts</h5>
            @foreach($cv_sections_data['contacts'] as $field_name => $contacts)
                <div class="col-12">
                    <p> <label class="form-control-label">Contact Email</label> <span> {{$contacts['contact_email']}} </span></p>
                    <p> <label class="form-control-label">Contact Phone</label> <span> {{$contacts['contact_phone']}} </span></p>
                </div>
            @endforeach
        </div>
    @endif

    @if(isset($cv_sections_data['work_experience']))
        <div class="row form-group" id="work_experience">
            <h5 class="presection-header">Work Experience</h5>
            @foreach($cv_sections_data['work_experience'] as $field_name => $work_experience)
                <div class="col-12">
                    <p> <label class="form-control-label">Company name</label> <span> {{$work_experience['company_name']}} </span></p>
                    <p> <label class="form-control-label">Work position</label> <span> {{$work_experience['work_position']}} </span></p>
                </div>
            @endforeach
        </div>
    @endif

    @if(isset($cv_sections_data['skills']))
        <div class="row form-group" id="skills">
            <h5 class="presection-header">Skills</h5>
            <div class="col-12" >
                @foreach($cv_sections_data['skills'] as $field_name => $skills)
                    <span style="background-color: #4f90cd; border-radius: 35px; padding: 20px;">{{$skills['skill_name']}} </span>
                @endforeach
            </div>
        </div>
    @endif 




</body>
</html>